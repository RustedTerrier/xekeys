# XEKEYS

A simple program to gather key-presses.

## Note: 
* None of the mod keys are case sensitive.
* Ctrl does not work on '-', ';', ''', ',', '.', '[' or '+'.
* Super only supports arrow keys as I found no way to support regular keys. Any pull requests on this issue would be appreciated.
* Raw mode is needed for most of the key detection.


## Simple example

```rust
extern crate xekeys;

use xekeys::{get_keys, Keys};

fn main() {
    loop {
        // Here we get the keypresses as a struct.
        let keys = get_keys();

        // Here we check if q is pressed and control is pressed and if so, we break.
        match keys {
            | Keys::CtrlChar('q') => break,
            | _ => {}
        };
    }
}
```

## Motivation

When I was using termion, I came to the realization that I was unabled to detect ctrl+arrow keys, something I needed. So I made my own crate; but, to justify using this over termion, I added most of the functionality of termion key presses and some extra, such as more modifiers with arrow keys and function keys up to 20.

## License

XEKEYS uses BSD-3 clause.

## Termion Alternative:
I dont want to make a full termion alternative, termion is already great; but, if you want one, I suggest using this for keypresses, [raw_tty](https://lib.rs/crates/raw_tty) for raw mode, [lliw](https://github.com/curlpipe/lliw) for styling, and I guess you can do the cursor movement using ANSI escape codes as well as clearing.

## More Info:

* [Usage](https://codeberg.org/RustedTerrier/xekeys/wiki/Usage/)
