#![doc(html_root_url = "https://codeberg.org/RustedTerrier/xekeys/wiki")]
#![feature(exclusive_range_pattern)]
pub fn get_keys() -> Keys {
    use std::{io, io::Read};
    let mut keys = io::stdin().bytes();
    match keys.next().transpose().unwrap() {
        | Some(byte) => match byte {
            | b'\x1b' => match keys.next().transpose().unwrap() {
                | Some(b'[') => match keys.next().transpose().unwrap() {
                    | Some(b'1') => {
                        let mut next = keys.next().transpose().unwrap();
                        if next == Some(b';') {
                            next = keys.next().transpose().unwrap();
                        }
                        match next {
                            // Super
                            | Some(b'1') => match keys.next().transpose().unwrap() {
                                | Some(b'A') => Keys::SuperArrow(ArrowKey::Up),
                                | Some(b'B') => Keys::SuperArrow(ArrowKey::Down),
                                | Some(b'C') => Keys::SuperArrow(ArrowKey::Right),
                                | Some(b'D') => Keys::SuperArrow(ArrowKey::Left),
                                | _ => Keys::None
                            },
                            // Shift
                            | Some(b'2') => match keys.next().transpose().unwrap() {
                                | Some(b'A') => Keys::ShiftArrow(ArrowKey::Up),
                                | Some(b'B') => Keys::ShiftArrow(ArrowKey::Down),
                                | Some(b'C') => Keys::ShiftArrow(ArrowKey::Right),
                                | Some(b'D') => Keys::ShiftArrow(ArrowKey::Left),
                                | _ => Keys::None
                            },
                            // Alt
                            | Some(b'3') => match keys.next().transpose().unwrap() {
                                | Some(b'A') => Keys::AltArrow(ArrowKey::Up),
                                | Some(b'B') => Keys::AltArrow(ArrowKey::Down),
                                | Some(b'C') => Keys::AltArrow(ArrowKey::Right),
                                | Some(b'D') => Keys::AltArrow(ArrowKey::Left),
                                | _ => Keys::None
                            },
                            // Super and Shift
                            | Some(b'4') => match keys.next().transpose().unwrap() {
                                | Some(b'A') => Keys::ShiftSuperArrow(ArrowKey::Up),
                                | Some(b'B') => Keys::ShiftSuperArrow(ArrowKey::Down),
                                | Some(b'C') => Keys::ShiftSuperArrow(ArrowKey::Right),
                                | Some(b'D') => Keys::ShiftSuperArrow(ArrowKey::Left),
                                | _ => Keys::None
                            },
                            // Control
                            | Some(b'5') => match keys.next().transpose().unwrap() {
                                | Some(b'A') => Keys::CtrlArrow(ArrowKey::Up),
                                | Some(b'B') => Keys::CtrlArrow(ArrowKey::Down),
                                | Some(b'C') => Keys::CtrlArrow(ArrowKey::Right),
                                | Some(b'D') => Keys::CtrlArrow(ArrowKey::Left),
                                | Some(b'~') => Keys::Function(5),
                                | _ => Keys::None
                            },
                            // Control and Shift
                            | Some(b'6') => match keys.next().transpose().unwrap() {
                                | Some(b'A') => Keys::CtrlShiftArrow(ArrowKey::Up),
                                | Some(b'B') => Keys::CtrlShiftArrow(ArrowKey::Down),
                                | Some(b'C') => Keys::CtrlShiftArrow(ArrowKey::Right),
                                | Some(b'D') => Keys::CtrlShiftArrow(ArrowKey::Left),
                                | _ => Keys::None
                            },
                            // Alt + Control
                            | Some(b'7') => match keys.next().transpose().unwrap() {
                                | Some(b'A') => Keys::CtrlAltArrow(ArrowKey::Up),
                                | Some(b'B') => Keys::CtrlAltArrow(ArrowKey::Down),
                                | Some(b'C') => Keys::CtrlAltArrow(ArrowKey::Right),
                                | Some(b'D') => Keys::CtrlAltArrow(ArrowKey::Left),
                                | Some(b'~') => Keys::Function(6),
                                | _ => Keys::None
                            },
                            // Control + Alt + Shift
                            | Some(b'8') => match keys.next().transpose().unwrap() {
                                | Some(b'A') => Keys::CtrlAltShiftArrow(ArrowKey::Up),
                                | Some(b'B') => Keys::CtrlAltShiftArrow(ArrowKey::Down),
                                | Some(b'C') => Keys::CtrlAltShiftArrow(ArrowKey::Right),
                                | Some(b'D') => Keys::CtrlAltShiftArrow(ArrowKey::Down),
                                | Some(b'~') => Keys::Function(7),
                                | _ => Keys::None
                            },
                            | Some(b'9') => match keys.next().transpose().unwrap() {
                                | Some(b'~') => Keys::Function(8),
                                | _ => Keys::None
                            },
                            | _ => Keys::Esc
                        }
                    },
                    | Some(b'2') => match keys.next().transpose().unwrap() {
                        | Some(b'0') => match keys.next().transpose().unwrap() {
                            | Some(b'~') => Keys::Function(9),
                            | _ => Keys::None
                        },
                        | Some(b'1') => match keys.next().transpose().unwrap() {
                            | Some(b'~') => Keys::Function(10),
                            | _ => Keys::None
                        },
                        | Some(b'3') => match keys.next().transpose().unwrap() {
                            | Some(b'~') => Keys::Function(11),
                            | _ => Keys::None
                        },
                        | Some(b'4') => match keys.next().transpose().unwrap() {
                            | Some(b'~') => Keys::Function(12),
                            | _ => Keys::None
                        },
                        | Some(b'5') => match keys.next().transpose().unwrap() {
                            | Some(b'~') => Keys::Function(13),
                            | _ => Keys::None
                        },
                        | Some(b'6') => match keys.next().transpose().unwrap() {
                            | Some(b'~') => Keys::Function(14),
                            | _ => Keys::None
                        },
                        | Some(b'7') => match keys.next().transpose().unwrap() {
                            | Some(b';') => {
                                let a = keys.next().transpose().unwrap();
                                let b = keys.next().transpose().unwrap();
                                let c = keys.next().transpose().unwrap();
                                let d = keys.next().transpose().unwrap();
                                let e = keys.next().transpose().unwrap();
                                match (a, b, c, d, e) {
                                    // Technically it's shift+enter
                                    | (Some(b'2'), Some(b';'), Some(b'1'), Some(b'3'), Some(b'~')) => Keys::ShiftEnter,
                                    | (Some(b'5'), Some(b';'), Some(b'1'), Some(b'3'), Some(b'~')) => Keys::CtrlChar('\n'),
                                    | _ => Keys::None
                                }
                            },
                            | _ => Keys::None
                        },
                        | Some(b'8') => match keys.next().transpose().unwrap() {
                            | Some(b'~') => Keys::Function(15),
                            | _ => Keys::None
                        },
                        | Some(b'9') => match keys.next().transpose().unwrap() {
                            | Some(b'~') => Keys::Function(16),
                            | _ => Keys::None
                        },
                        | _ => Keys::None
                    },
                    | Some(b'3') => match keys.next().transpose().unwrap() {
                        | Some(b'~') => Keys::Backspace,
                        | Some(b';') => match keys.next().transpose().unwrap() {
                            | Some(b'3') | Some(b'5') => match keys.next().transpose().unwrap() {
                                | Some(b'~') => Keys::AltBackspace,
                                | _ => Keys::None
                            },
                            | _ => Keys::None
                        },
                        | Some(b'1') => match keys.next().transpose().unwrap() {
                            | Some(b'~') => Keys::Function(17),
                            | _ => Keys::None
                        },
                        | Some(b'2') => match keys.next().transpose().unwrap() {
                            | Some(b'~') => Keys::Function(18),
                            | _ => Keys::None
                        },
                        | Some(b'3') => match keys.next().transpose().unwrap() {
                            | Some(b'~') => Keys::Function(19),
                            | _ => Keys::None
                        },
                        | Some(b'4') => match keys.next().transpose().unwrap() {
                            | Some(b'~') => Keys::Function(20),
                            | _ => Keys::None
                        },
                        | _ => Keys::None
                    },
                    | Some(b'5') => match keys.next().transpose().unwrap() {
                        | Some(b'~') => Keys::PageUp,
                        | _ => Keys::None
                    },
                    | Some(b'6') => match keys.next().transpose().unwrap() {
                        | Some(b'~') => Keys::PageDown,
                        | _ => Keys::None
                    },
                    // Arrow keys
                    | Some(b'A') => Keys::Arrow(ArrowKey::Up),
                    | Some(b'B') => Keys::Arrow(ArrowKey::Down),
                    | Some(b'C') => Keys::Arrow(ArrowKey::Right),
                    | Some(b'D') => Keys::Arrow(ArrowKey::Left),
                    | _ => Keys::Esc
                },
                | Some(b'O') => match keys.next().transpose().unwrap() {
                    | Some(b'P') => Keys::Function(1),
                    | Some(b'Q') => Keys::Function(2),
                    | Some(b'R') => Keys::Function(3),
                    | Some(b'S') => Keys::Function(4),
                    | _ => Keys::None
                },
                | Some(b'\x1B') => Keys::Esc,
                | Some(b'\x7f') => Keys::AltBackspace,
                // I change it to \n because that's more intuitive(at least in my opinion)
                | Some(b'\r') | Some(b'\n') => Keys::AltChar('\n'),
                | Some(byte @ _) => Keys::AltChar(byte as char),
                | None => Keys::None
            },
            | _ => {
                match byte {
                    // I change it to \n because that's more intuitive(at least in my opinion)
                    | b'\r' | b'\n' => Keys::Char('\n'),
                    | b'\t' => Keys::Char('\t'),
                    | _ => match byte {
                        | b'\x7f' => Keys::Backspace,
                        | b'\x00' .. b'\x1c' => Keys::CtrlChar((byte + 96) as char),
                        | b'\x1c' ..= b'\x1f' => Keys::CtrlChar((byte + 64) as char),
                        | byte @ _ => Keys::Char(byte as char)
                    }
                }
            }
        },
        | None => Keys::None
    }
}

pub enum Keys {
    Char(char),
    CtrlChar(char),
    AltChar(char),
    Arrow(ArrowKey),
    CtrlArrow(ArrowKey),
    AltArrow(ArrowKey),
    SuperArrow(ArrowKey),
    ShiftArrow(ArrowKey),
    CtrlShiftArrow(ArrowKey),
    AltShiftArrow(ArrowKey),
    CtrlAltArrow(ArrowKey),
    ShiftSuperArrow(ArrowKey),
    CtrlAltShiftArrow(ArrowKey),
    Function(u8),
    Esc,
    PageUp,
    PageDown,
    Backspace,
    AltBackspace,
    ShiftEnter,
    None
}

pub enum ArrowKey {
    Up,
    Down,
    Right,
    Left
}
